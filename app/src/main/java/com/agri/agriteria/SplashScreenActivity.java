package com.agri.agriteria;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {

    private ImageView ivTray, ivCup, ivSmoke;
    private TextView tvAgriTeria;
    private Animation trayAnim, smokeAnim;
    private static final long MY_SPLASH_TIME_DELAY = 2000;

    private void initViews() {
        ivTray = findViewById(R.id.ivTray);
        ivCup = findViewById(R.id.ivCup);
        ivSmoke = findViewById(R.id.ivSmoke);
        tvAgriTeria = findViewById(R.id.ivAppName);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        initViews();
        Utils.hideActionBar(this);
        startAnimation();

    }

    private void startAnimation() {
        ivCup.setVisibility(View.INVISIBLE);
        ivSmoke.setVisibility(View.INVISIBLE);
        tvAgriTeria.setVisibility(View.INVISIBLE);

        trayAnim = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.slide_up);
        ivTray.startAnimation(trayAnim);

        trayAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation cupAnimation) {
                ivCup.setVisibility(View.VISIBLE);

                ivCup.animate().rotationY(360).setDuration(550).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ivSmoke.setVisibility(View.VISIBLE);

                        smokeAnim = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.fade_in);
                        ivSmoke.startAnimation(smokeAnim);

                        smokeAnim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation slideUpAppNameAnim) {
                                tvAgriTeria.setVisibility(View.VISIBLE);
                                slideUpAppNameAnim = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.slide_up);
                                tvAgriTeria.startAnimation(slideUpAppNameAnim);
                                splashPostDelayed();
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void splashPostDelayed() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class));
                    finish();
            }
        }, MY_SPLASH_TIME_DELAY);
    }

    @Override
    public void onBackPressed() {
        // empty for no back in splash...
    }
}