package com.agri.agriteria;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Utils.hideActionBar(this);
    }

    public void goToLoginActivity(View view) {
        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
    }
}