package com.agri.agriteria;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class Utils {

    public static void hideActionBar(AppCompatActivity appCompatActivity) {
        ActionBar actionBar = appCompatActivity.getSupportActionBar();
        if(actionBar != null) {
            actionBar.hide();
        }
    }
}
